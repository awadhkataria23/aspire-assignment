import Vue from "vue";
import Router from "vue-router";
import Dashboard from "./components/Dashboard";

Vue.use(Router);

let router = new Router({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "dashboard",
      component: Dashboard,
    },
  ],
});
// router.beforeEach((to, from, next) => {});
export default router;
